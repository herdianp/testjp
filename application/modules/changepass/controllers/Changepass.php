<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Changepass extends MX_Controller {
	private $smscreate;

	function __construct()
	{
		parent::__construct();
		$is_logged_in = $this->session->userdata("login");
		if(!isset($is_logged_in) || $is_logged_in != TRUE){	
			redirect('login', 'refresh');
		}		
		$this->mstemplate = $this->uri->segment(2);
		$this->load->model('Mchangepass');
		$this->load->model('Object_model');
		
	}
	
	public function index()
	{	
		$data['title'] = 'Welcome to Admin Tools aplikasi Velis SMS Broadcast ';
		$data['description'] = 'Welcome to Tools Admin aplikasi Velis SMS Broadcast';
		$data['loadjs'] = 'home.js';
		$data['loadjs'] = 'changepass.js';
		$data['loadjs'] = 'jquery.js';
		$data['loadjs'] = 'jquery.form.min.js';
		$this->load->library('template');
		$this->template->display("changepass", $data);
		$data['id'] = $this->session->userdata['login']['id'];


	}


	public function execute() 
	{
		$name 		= $this->session->userdata['login']['name'];
		$id 		= $this->session->userdata['login']['id'];

		$dataUser = $this->Mchangepass->dtusr($name, $id);

		//print_r($dataUser); die();
		$pass = (object)array(
		'password'	=> md5($this->input->post('new')),
		);

		$where = array(
		'name'		=> $name,
		'id'		=> $id
		);



		$passOld = md5($this->input->post('old'));
		
		//get data for log
		$passOldlog = $this->input->post('old');
		$passNewlog = $this->input->post('new');
		$re = $this->input->post('re');
		$pasdb = $dataUser->password;

		// echo $pasdb; die();
		if($passOld == $pasdb AND $passNewlog == $re){
			$this->Mchangepass->updatePass("mahasiswa", $pass, $where);
			echo 'sukses';
			
		}else{
			echo 'gagal';
		}
	}

}
