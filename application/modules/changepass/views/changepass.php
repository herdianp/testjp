<?php $sessionId = session_id();?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-maxlength/1.7.0/bootstrap-maxlength.min.js"></script>
<!-- Main content -->
<section class="content">
  <div class="row">
    <!-- left column -->
    <div class="col-md-6">
      <!-- general form elements -->
      <div class="box box-primary">
        <div class="box-header with-border">
          <h3 class="box-title">Change Password</h3>
        </div><!-- /.box-header -->
        <!-- form start -->
       
        <form id="form1" role="form" method="POST" action="changepass/execute/" enctype="multipart/form-data">
          <div class="box-body">
            
            <div class="form-group">
              <label for="smssubject">Old Passwowrd</label>
              <input type="text" id="old" name="old" value="" placeholder="Old Passwowrd" class="form-control"/>
            </div>


            <div class="form-group">
              <label for="smssubject">New Passwowrd</label>
              <input type="text" id="new" name="new" value="" placeholder="New Passwowrd" class="form-control"/>
            </div>
           
            <div class="form-group">
              <label for="smssubject">Re-Type New Passwowrd</label>
              <input type="text" id="re" name="re" value="" placeholder="Re-Type New Passwowrd" class="form-control"/>
            </div>    

          </div><!-- /.box-body -->

          <div class="box-footer">
            <button type="submit" id="submit" class="btn btn-info pull-right">Submit</button>
          </div><!-- /.box-footer -->
        </form>
      </div><!-- /.box -->

    </div><!--/.col (left) -->
    <!-- right column -->
    <div class="col-md-6">
      <!-- Horizontal Form -->
      <div class="box box-warning">
        <div class="box-header with-border">
          <h3 class="box-title">User Information</h3>
        </div><!-- /.box-header -->
        <!-- form start -->
        
          <div class="box-body">
            <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label" style="font-weight: 100;">Username </label>
              <div class="col-sm-9">
               <label for="inputEmail3" class="col-sm-3 control-label"><?=$name?> </label>
              </div>
            </div>
            
          </div><!-- /.box-body -->
          
        
       
      </div><!-- /.box -->
      
    </div><!--/.col (right) -->
  </div>   <!-- /.row -->
</section><!-- /.content -->

<script type="text/javascript">
$('textarea').maxlength({
      alwaysShow: true,
      threshold: 10,
      warningClass: "label label-success",
      limitReachedClass: "label label-danger",
      separator: ' out of ',
      preText: 'You write ',
      postText: ' chars.',
      validate: true
});

function notifSuccess(){
	 new PNotify({
	    title: 'SUCCESS ADD DATA',
	    text: 'Password has been change',
	    type: 'success',
	    animate: {
	        animate: true,
	        in_class: 'bounceInLeft',
        	out_class: 'bounceOutRight'
	    }
	});
}
function notifError(){
	 new PNotify({
	    title: 'FAILED DATA',
	    text: 'Your old password is wrong',
	    type: 'error',
	    animate: {
	        animate: true,
	        in_class: 'bounceInLeft',
        	out_class: 'bounceOutRight'
	    }
	});
}
function notifError2(){
	 new PNotify({
	    title: 'FAILED DATA',
	    text: 'New password is required',
	    type: 'error',
	    animate: {
	        animate: true,
	        in_class: 'bounceInLeft',
        	out_class: 'bounceOutRight'
	    }
	});
}
function notifError3(){
	 new PNotify({
	    title: 'FAILED DATA',
	    text: 'Re-type new password is required',
	    type: 'error',
	    animate: {
	        animate: true,
	        in_class: 'bounceInLeft',
        	out_class: 'bounceOutRight'
	    }
	});
}
function notifError4(){
	 new PNotify({
	    title: 'FAILED DATA',
	    text: 'Re-type new password does not match the new password',
	    type: 'error',
	    animate: {
	        animate: true,
	        in_class: 'bounceInLeft',
        	out_class: 'bounceOutRight'
	    }
	});
}

$("#form1").submit(function( event ) {
  event.preventDefault();
  $('#submit').text('Process...'); //change button text
  $('#submit').attr('disabled',true); //set button enable
 $(this).ajaxSubmit({                          
  success: function(resp) {
 
  //alert(resp);
  $('#submit').text('Submit'); //change button text
  $('#submit').attr('disabled',false); //set button enable
     
    if ($('#new').val() == "") {
	notifError2();
    }
    else if ($('#re').val() == "") {
	notifError3();
    }
    else if ($('#re').val() != $('#new').val()) {
	notifError4();
    }
    else {
    	if(resp == "sukses"){
     	   notifSuccess();
          setTimeout(function () {
              window.location.assign("login/logout");
          }, 2000);
    	}else{
      	   notifError();
      
	   //console.log(resp); 
       }
    }
  }
});



});
</script>