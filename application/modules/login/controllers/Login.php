<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MX_Controller {
	private $pageinfo; //Setting Page Information
	public $status; //status response from request ajax
	public $message; // message response request ajax
	
	public  function __construct(){
        parent::__construct();
		
		$this->pageinfo['title'] = 'Login';
		$this->pageinfo['description'] = 'Form Login Mahasiswa';
		$this->pageinfo['loadjs'] = 'login.js';	
		
		//$this->load->library("rest"); //load library rest
    }
	
	public function index()
	{		
		$is_logged_in = $this->session->userdata("logged_in");
		if(isset($is_logged_in) || $is_logged_in == TRUE){	
			redirect('home');
		}				
		
		$data = $this->pageinfo;
		$this->load->view('login', $data);
		
	}
	
	public function do_login(){
		$this->load->helper('security');
		$this->load->library('form_validation');
		$warning = "<b>Warning : </b>";
		$invalid = 'Invalid Email or Password';	
		$this->message = $warning.$invalid;
        $this->form_validation->set_rules('email', 'email', 'trim|required|xss_clean');
        $this->form_validation->set_rules('password', 'password', 'trim|required|xss_clean');
		if($this->form_validation->run() == FALSE){
			$replace = array("<p>","</p>");
			$error = str_replace($replace,"",validation_errors());	
			$this->message = $warning.$error;
			$this->status = FALSE;	
        }else{  
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$result = $this->authentication->check_login($email, $password); //do check login 
			$this->status = $result->sts;
			$this->message = $result->msg?$result->msg:$this->message;		
        }
		
		echo json_encode(array("status" => $this->status,
							   "message" => $this->message));
    }
 
    public function Logout(){
        $this->session->sess_destroy();
		redirect('login');
    }
}
