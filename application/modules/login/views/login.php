
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?=$title;?></title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<meta name="description" content="<?=$description;?>" />
		
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?=THEME;?>bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?=THEME;?>bootstrap/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?=THEME;?>bootstrap/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?=THEME;?>dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?=THEME;?>plugins/iCheck/square/blue.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition login-page">
    <div class="login-box">
      <div class="login-box-body">
        <p class="login-box-logo"><center><h2>Login Mahasiswa</h2></center></p>
        <form role="form" id="loginForm" method="POST" action="<?=base_url('login/do_login');?>">
          <div id="alert" class="alert alert-danger alert-dismissable hide"></div>
		  <div class="form-group has-feedback">
		    <input id="redirect" type="hidden" value="<?=base_url();?>">
            <input type="text" name="email" class="form-control" placeholder="username" autofocus>
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" name="password" class="form-control" placeholder="Password">
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-8">            
            </div><!-- /.col -->
            <div class="col-xs-4">
              <button type="submit" id="dosubmit" class="btn btn-default btn-block">Log In</button>
            </div><!-- /.col -->
          </div>
        </form>	
		  
      </div><!-- /.login-box-body -->
	 
	
    <!-- jQuery 2.1.4 -->
    <script src="<?=THEME;?>plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?=THEME;?>bootstrap/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="<?=THEME;?>plugins/iCheck/icheck.min.js"></script>
	<!-- Login Javascript -->
    <script src="<?=JS.$loadjs;?>"></script>
  </body>
</html>
