<section class="content-header">
    <h1>
        Data Tables
        <small>advanced tables</small>
	</h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Data tables</li>
	</ol>
</section>
<!-- Main content -->
<section class="content">
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
                <div class="box-header">
					<h3 class="box-title">User Management</h3>
					<button class="btn btn-default btn-sm pull-right" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Reload</button>
					<div class="pull-right">&nbsp;</div>
					<button class="btn btn-primary btn-sm pull-right" onclick="openModalAdd()"><i class="fa fa-plus" aria-hidden="true"></i> Add Data</button>
                </div><!-- /.box-header -->
				<!-- Modal -->
				<div class="container">
				<div class="modal fade" id="myModal" role="dialog">
                    <div class="modal-dialog">
						<!-- Modal content-->
                        <div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title"></h4>
                            </div>
							
                            <div class="modal-body form">
								<form action="#" id="formModalAdd" class="form-horizontal">
								<div class="form-body">
									<div class="form-group">
										<label for="email" class="col-sm-2 control-label">No Induk</label>
										<div class="col-sm-10">
											<input type="hidden" id="id" name="id" />
											<input type="text" id="induk" name="induk" placeholder="induk" class="form-control"/>
											<span class="help-block"></span>
										</div>
									</div>

									<div class="form-group">
										<label for="name" class="col-sm-2 control-label">Name</label>
										<div class="col-sm-10">
											<input type="text" id="name" name="name" placeholder="Name" class="form-control"/>
											<span class="help-block"></span>
										</div>
									</div>

									<div class="form-group">
										<label for="name" class="col-sm-2 control-label">kelas</label>
										<div class="col-sm-10">
											<input type="text" id="kelas" name="kelas" placeholder="kelas" class="form-control"/>
											<span class="help-block"></span>
										</div>
									</div>

									<div class="form-group">
										<label for="name" class="col-sm-2 control-label">umur</label>
										<div class="col-sm-10">
											<input type="text" id="umur" name="umur" placeholder="umur" class="form-control"/>
											<span class="help-block"></span>
										</div>
									</div>

									<div class="form-group">
										<label for="password" id="label-password" class="col-sm-2 control-label">Password</label>
										<div class="col-sm-10">
											<input type="password" id="password" name="password" placeholder="Password" class="form-control" required="" />
											<span class="help-block"></span>
										</div>	
									</div>
								</div>	
								</form>
                            </div>
							
							<div class="modal-footer">
								<button type="Submit" id="btnSave" class="btn btn-success" onclick="save()">Submit</button>
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
				</div>
				<div class="container">
				<div class="modal fade" id="myModalDelete" role="dialog">
                    <div class="modal-dialog">
						<!-- Modal content-->
                        <div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Delete Data User</h4>
                            </div>
							
                            <div class="modal-body form">
								<p class="form-control-static">Are you sure want to delete this user <b><i><span class="deleteUser"></span></i></b> ?</p>
                            </div>
							
							<div class="modal-footer">
								<button type="Submit" id="btnDelete" class="btn btn-success" onclick="save()">Delete</button>
								<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>
				</div>
				<!--//Modal-->
                <div class="box-body">
				
                  <table id="table" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th width="10">No</th>
                        <th>ID</th>
                        <th>Induk</th>
                        <th>Name</th>
                        <th>Kelas</th>
                        <th>Umur(tahun)</th>
                        <!--<th>Password</th>-->
                        <th>Action</th>
                      </tr>
                    </thead>
                  </table>
				
                </div><!-- /.box-body -->
			</div><!-- /.box -->
        </div><!-- /.col -->
    </div><!-- /.row -->
</section><!-- /.content -->
