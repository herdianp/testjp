<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MX_Controller {
	private $site;
	function __construct()
	{
		parent::__construct();
		$is_logged_in = $this->session->userdata("login");
		if(!isset($is_logged_in) || $is_logged_in != TRUE){	
			redirect('login', 'refresh');
		}	
		$this->site = $this->uri->segment(2); 
		$this->load->model('Object_model');
		
	}
	
	public function index()
	{	
		$data['title'] = 'Welcome to Admin Tools aplikasi Velis SMS Broadcast ';
		$data['description'] = 'Welcome to Tools Admin aplikasi Velis SMS Broadcast';
		$data['loadjs'] = 'users_management.js';
		$this->load->library('template');
		$this->template->display("home", $data);
	}
	
	public function getDataTable(){
		//setting key memcached
		
		// $db_1 = $this->load->database('db_1', TRUE);
		
		// $db_1['db']['db_1'] = array('database' => 'db_1');
		
		// $query = $db_1->query('select * from tbluser')->result();
		
		$query['table_parent'] = 'mahasiswa';
		$query['table_column'] = array ('','id','induk','name','kelas','umur','');
		$query['table_result'] = 'datatable';
		$result = $this->Object_model->getTable($query);
		echo json_encode($result);
	}
	
	public function ajax_add()
    {
        $this->_validateAdd();
		$table = 'mahasiswa';
		
        $data = array(
                'induk' => $this->input->post('induk'),
                'name' => $this->input->post('name'),
                'kelas' => $this->input->post('kelas'),
                'umur' => $this->input->post('umur'),
                'password' => md5($this->input->post('password'))
            );
			
		$this->db->insert($table, $data);
        echo json_encode(array("status" => TRUE, "title" => 'Success!', "text" => 'The data has been successfully added to the database', "type" => 'info'));
    }
	
	// public function ajax_edit($id)
	// {
	// 	$data = $this->db->query('SELECT * FROM user WHERE id = '.$id.'')->row();
    //        // $data->dob = ($data->dob == '0000-00-00') ? '' : $data->dob; // if 0000-00-00 set tu empty for datepicker compatibility
    //        echo json_encode($data);
	// }
	
	public function ajax_update()
    {
        $this->_validate();
		$table = 'mahasiswa';
        $data = array(
                'induk' => $this->input->post('induk'),
                // 'password' => md5($this->input->post('password')),
                'name' => $this->input->post('name'),
                'kelas' => $this->input->post('kelas'),
                'umur' => $this->input->post('umur')
            );
		$where = array(
				'id' => $this->input->post('id')
				);
		$this->db->where($where);
		$this->db->update($table,$data);
        echo json_encode(array("status" => TRUE, "title" => 'Success!', "text" => 'The data has been successfully update to the database', "type" => 'info'));
    }
	
	public function ajax_delete () 
	{
		$table = 'mahasiswa';
		$where = array('id' => $this->input->post('id'));
		
		$this->db->where($where);
		$this->db->delete($table);
		echo json_encode(array("status" => TRUE, "title" => 'Success!', "text" => 'The data has been successfully deleted from the database', "type" => 'info'));
	}
	
	private function _validate()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if($this->input->post('induk') == '')
        {
            $data['inputerror'][] = 'induk';
            $data['error_string'][] = 'Induk is required';
            $data['status'] = FALSE;
        }

        if($this->input->post('name') == '')
        {
            $data['inputerror'][] = 'name';
            $data['error_string'][] = 'Name is required';
            $data['status'] = FALSE;
        }

        if($this->input->post('kelas') == '')
        {
            $data['inputerror'][] = 'kelas';
            $data['error_string'][] = 'Kevel is required';
            $data['status'] = FALSE;
        }

         if($this->input->post('umur') == '')
        {
            $data['inputerror'][] = 'umur';
            $data['error_string'][] = 'Umur is required';
            $data['status'] = FALSE;
        }

        if($data['status'] === FALSE)
        {
            echo json_encode($data);
            exit();
        }
    }

    private function _validateAdd()
    {
        $data = array();
        $data['error_string'] = array();
        $data['inputerror'] = array();
        $data['status'] = TRUE;

        if($this->input->post('induk') == '')
        {
            $data['inputerror'][] = 'induk';
            $data['error_string'][] = 'Induk is required';
            $data['status'] = FALSE;
            $data['status'] = FALSE;
		}

        if($this->input->post('name') == '')
        {
            $data['inputerror'][] = 'name';
            $data['error_string'][] = 'Name is required';
            $data['status'] = FALSE;
        }

        if($this->input->post('kelas') == '')
        {
            $data['inputerror'][] = 'kelas';
            $data['error_string'][] = 'Kelas is required';
            $data['status'] = FALSE;
        }

        if($this->input->post('umur') == '')
        {
            $data['inputerror'][] = 'umur';
            $data['error_string'][] = 'Umur is required';
            $data['status'] = FALSE;
        }

        if($this->input->post('password') == '')
        {
            $data['inputerror'][] = 'password';
            $data['error_string'][] = 'password is required';
            $data['status'] = FALSE;
        }

        if($data['status'] === FALSE)
        {
            echo json_encode($data);
            exit();
        }
    }
}
