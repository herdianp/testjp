<?php
class Mhome extends CI_Model{
    var $table = 'user';
	var $order = array('id' => 'desc'); // default order
	
    function getAll()
    {
		$this->db->select('*');
        $this->db->from('user');
        return $this->db->get();
    }
}