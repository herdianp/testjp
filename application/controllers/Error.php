<?php
class Error extends Controller {
 
	function __construct()
	{
		parent::__construct();
		$this->load->library("airnav"); //load library airnav
	}
	
	function error_404()
	{
		$this->output->set_status_header('404');
		$this->airnav->load_miss_page();
	}
}