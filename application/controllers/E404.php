<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class E404 extends CI_Controller {
 
	function index()
	{
		$d['title'] = 'Maaf, halaman tidak ditemukan';
		$this->load->view('errors/html/e404',$d);
	}
}