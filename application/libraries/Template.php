<?php
class Template {
	protected $_ci;
	function __construct()
	{
		$this->_ci=&get_instance();
	}

	function show_menu()
	{
		$menu = array();
		$menu[] = array("name" => "Dashboard",
						"link" => "home",
						"icon" => "fa-user");
		$menu[] = array("name" => "Change Password",
						"link" => "changepass",
						"icon" => "fa-key");	
		return (empty($menu)) ? "NULL" : $menu;
	}
	
	function display($template,$data=null)
	{
		if(!$this->is_ajax())
		{
			
			$data['name'] 			= $this->_ci->session->userdata['login']['name'];
			$data['content']		= $this->_ci->load->view($template,$data, true);
			$data['menus']			= $this->show_menu();
			$data['menu_top']		= $this->_ci->load->view('layout/menu-top',$data, true);
			$data['header']			= $this->_ci->load->view('layout/header',$data, true);
			$data['menu_left']		= $this->_ci->load->view('layout/menu-left',$data, true);			
			$data['footer']			= $this->_ci->load->view('layout/footer',$data, true);
			
			$this->_ci->load->view('/layout/index.php', $data);
		}
		else
		{
			$this->_ci->load->view($template,$data);
		}
	}

	function is_ajax()
	{
		return
		($this->_ci->input->server('HTTP_X_REQUESTED_WITH')&&($this->_ci->input->server('HTTP_X_REQUESTED_WITH')=='XMLHttpRequest'));
	}
}