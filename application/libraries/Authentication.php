<?php if(!defined('BASEPATH')) exit('No direct access script allowed');

class authentication
{
	public $sts;
	public $msg;
	function __construct()
	{
		$this->ci = &get_instance();
	}
	
	// untuk validasi login
	public function check_login($email, $pass)
	{
		$this->ci->load->model("Object_model");
		$Cuser = array('from' => "mahasiswa",
					   'where' => array("name" 	=> $email,
					   					"password" 	=> md5($pass))
									);		
		$Res_Cuser = $this->ci->Object_model->get_data($Cuser);		
		if($Res_Cuser){
			$Clogin = array('from' => "mahasiswa",
					   		'where' => array("name" 	=> $email,
											 "password" => md5($pass))
										);		
			$Res_Clogin = $this->ci->Object_model->get_data($Clogin);
			if($Res_Cuser){
				$session_data = array();
				$session_data = array('id' => $Res_Cuser->id,
									  'name' => $Res_Cuser->name);
				$this->ci->session->set_userdata('login', $session_data);
				$this->sts = TRUE;	
				$this->msg = "Selamat Datang {$Res_Cuser->name}";
			}else{
				$this->sts = FALSE;
				$this->msg = "Invalid your Email or password ! ";
			}	
		}else{
			$this->sts = FALSE;
			$this->msg = 'Invalid your Email or password !';
		}		
		return $this;
	}
	
	// cek apakah user login apa belum
	public function is_logged_in()
	{
		if($this->ci->session->userdata('usr_id') == '')
		{
			return FALSE;
		}
		return TRUE;
	}

	
	

}
