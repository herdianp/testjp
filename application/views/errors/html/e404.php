<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AdminLTE 2 | 404 Page not found</title>
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?=THEME;?>bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?=THEME;?>bootstrap/css/font-awesome.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?=THEME;?>dist/css/AdminLTE.min.css">

  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">
      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper" style="margin-left: 0px; position: relative;">
        <!-- Main content -->
        <section class="content">
          <div class="error-page">
            <h2 class="headline text-yellow"> 404</h2>
            <div class="error-content">
              <h3><i class="fa fa-warning text-yellow"></i> Oops! Page not found.</h3>
              <p>
                We could not find the page you were looking for.
                Meanwhile, you may return to dashboard with click button the bellow.
              </p>
             <!--  <form class="search-form">
                <div class="input-group">
                  <input type="text" name="search" class="form-control" placeholder="Search">
                  <div class="input-group-btn">
                    <button type="submit" name="submit" class="btn btn-warning btn-flat"><i class="fa fa-search"></i></button>
                  </div>
                </div>
              </form> -->
              <a href="<?=base_url();?>"><button type="submit" name="submit" class="btn btn-warning btn-flat"><i class="fa fa-arrow-left"></i> Return to dashboard</button></a>
            </div><!-- /.error-content -->
          </div><!-- /.error-page -->
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="<?=THEME;?>plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?=THEME;?>bootstrap/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="<?=THEME;?>plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?=THEME;?>plugins/datatables/dataTables.bootstrap.min.js"></script>
    
    <!-- AdminLTE App -->
    <script src="<?=THEME;?>dist/js/app.min.js"></script>
    <script src="<?=THEME;?>dist/js/aes.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?=THEME;?>dist/js/demo.js"></script>
    <!-- page script -->
    <script type="text/javascript" src="<?=ASSETS;?>js/<?=$loadjs;?>"></script>

  </body>
</html>
