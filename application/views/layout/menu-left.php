<!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
         
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
		  <li class="header">MAIN NAVIGATION</li>
		  <?php foreach($menus as $value){ ?>		  
			<li>
              <a href="<?=base_url("{$value['link']}");?>">
                <i class="fa <?=$value['icon'];?>"></i> <span><?=$value['name'];?></span>
              </a>
            </li>		  
		  <?php }?>
                      </ul>
        </section>
        <!-- /.sidebar -->
      </aside>