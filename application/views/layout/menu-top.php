<ul class="nav navbar-nav">
	<li class="user-menu">
        <a href="#" >
            <img src="<?=THEME;?>dist/img/avatar.png" class="user-image" alt="User Image">
            <span class="hidden-xs"><b>Welcome,</b> <?=$name;?></span>
        </a>                
    </li>			
	<li class="user">
		<a  href="<?=base_url('login/logout');?>">
			<i class="fa fa-power-off"></i>
		</a>
	</li>
</ul>