<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?=$title;?></title>
	<meta name="description" content="<?=$description;?>" />
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?=THEME;?>bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?=THEME;?>bootstrap/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="<?=THEME;?>bootstrap/css/ionicons.min.css">
	  <!-- DataTables -->
    <link rel="stylesheet" href="<?=THEME;?>plugins/datatables/dataTables.bootstrap.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?=THEME;?>dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?=THEME;?>dist/css/skins/_all-skins.min.css">
    <!-- Pnotify -->
    <link rel="stylesheet" href="<?=THEME;?>plugins/pnotify/animate.css"> 
    <link rel="stylesheet" href="<?=THEME;?>plugins/pnotify/pnotify.custom.min.css">
    <!-- Include jQuery -->
    <script type="text/javascript" src="<?=ASSETS;?>bower_components/jquery/dist/jquery-1.11.3.min.js"></script>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  
   <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

      <?=$header;?>
      <?=$menu_left;?>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <?=$content;?>
      </div><!-- /.content-wrapper -->
     <?=$footer;?>

    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.4 -->
    <script src="<?=THEME;?>plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="<?=THEME;?>bootstrap/js/bootstrap.min.js"></script>
    <!-- DataTables -->
    <script src="<?=THEME;?>plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?=THEME;?>plugins/datatables/dataTables.bootstrap.min.js"></script>
    
    <!-- AdminLTE App -->
    <script src="<?=THEME;?>dist/js/app.min.js"></script>
    <script src="<?=THEME;?>dist/js/aes.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?=THEME;?>dist/js/demo.js"></script>
    <!-- page script -->
    <script type="text/javascript" src="<?=ASSETS;?>js/<?=$loadjs;?>"></script>

    <!-- Pnotify Js -->
    <script src="<?=THEME;?>plugins/pnotify/pnotify.custom.min.js"></script>  
  </body>
</html> 
