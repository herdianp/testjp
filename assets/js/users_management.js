var method; //for save method string
var table;

$.fn.dataTable.ext.buttons.alert = {
    className: 'buttons-alert',
 
    action: function ( e, dt, node, config ) {
        alert( this.text() );
    }
};

$(document).ready(function(){
	/**
	** Datatable
	*/
	table = $('#table').DataTable({
		"iDisplayLength": 15,
		"lengthMenu": [[15, 25, 50, -1], [15, 25, 50, "All"]],
		"columnDefs": 
		[
			{"targets": [0], "data": 'no', "searchable":false, "orderable":false, "width": "5%"},
			{"targets": [1], "visible": false,"searchable": false, "data":'id'},
			{"targets": [2], "data":'induk',"width": "18%"},
			{"targets": [3], "data":'name',"width": "25%"},
			{"targets": [4], "data":'kelas',"width": "13%"},
			{"targets": [5], "data":'umur',"width": "13%"},
			// {"targets": [5], "data":'password', "visible": false,"searchable": false},
			{
				"targets": [6],
				"className": "center",
				"width": "15%",
				"searchable": false,
				"orderable": false,
				"data": null,
				"defaultContent": assign_button_action(),
				"fnCreateCell": function(nTd, sData, oData, iRow, iCol)
				{
					$(".tooltip-button", nTd).tooltip({
					   selector: "[data-toggle=tooltip]",
					   container: "body"
					});
				}
			}
		],
		"order": [[ 1, "asc" ]],  
		"oLanguage": {"sProcessing": loader},
		"processing": true,
		"serverSide": true,
		"ajax": {
			"url":'home/getDataTable',
			"type": "POST"
		}
	});
	
	/**
	/* Editing Data
	**/
	
	$('#table').on( 'click', '#Edit', function () {
		method = 'update';
		$('#formModalAdd')[0].reset(); // reset form on modals
		$('.form-group').removeClass('has-error'); // clear error class
		$('.help-block').empty(); // clear error string
		$('#myModal').modal('show'); // show bootstrap modal
		$('.modal-title').text('Edit Data Users'); // Set Title to Bootstrap modal title
		$('#myModal').modal('show'); // show bootstrap modal
		
        var userList = table.row( $(this).parents('tr') ).data();
        $("#id").val(userList.id);
		$("#induk").val(userList.induk);
		$("#name").val(userList.name);
		$("#kelas").val(userList.kelas);
		$("#umur").val(userList.umur);
		$("#label-password").hide();
		$("#password").hide();
    } );
	
	/**
	/* Deleting Data
	**/
	
	$('#table').on( 'click', '#Delete', function () {
		method = 'delete';
		// $('#formModalAdd')[0].reset(); // reset form on modals
		$('.form-group').removeClass('has-error'); // clear error class
		$('.help-block').empty(); // clear error string
		$('#myModalDelete').modal('show'); // show bootstrap modal
		$('.modal-title').text('Delete Data Users'); // Set Title to Bootstrap modal title
		
        var userList = table.row( $(this).parents('tr') ).data();
		$("#id").val(userList.id);
		$("span.deleteUser").html(userList.name);		
    } );
	
	/**
	/* set input/textarea/select event when change value, remove class error and remove text help block 
	**/
	$("input").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
    
    $("select").change(function(){
        $(this).parent().parent().removeClass('has-error');
        $(this).next().empty();
    });
	
	/**
	/* Icon Loader
	**/
	function loader()
	{
		var load = "<i class=\'fa fa-spinner fa-3x fa-spin\'></i>";
		return load;
	}
});

function assign_button_action(data){
	var btn = '<button type=\"button\" id=\"Edit\" class=\"btn btn-success btn-xs\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Edit\"><i class=\"fa fa-edit\"></i> Edit</button>'+'&nbsp;&nbsp;&nbsp;'+'<button type=\"button\" id=\"Delete\" class=\"btn btn-danger btn-xs\" data-toggle=\"tooltip\" data-placement=\"right\" title=\"Delete\"><i class=\"fa fa-trash-o\"></i> Delete</button>';
	return btn;   
}

//**
/* Adding Data
**/
	
function openModalAdd()
{
	method = 'add';
	$('#formModalAdd')[0].reset(); // reset form on modals
	$('.form-group').removeClass('has-error'); // clear error class
	$('.help-block').empty(); // clear error string
	$('#myModal').modal('show'); // show bootstrap modal
	$('.modal-title').text('Add Data Mahasiswa'); // Set Title to Bootstrap modal title
	$("#label-password").show();
	$("#password").show();
}

function save()
{
	$('#btnSave').text('Process...'); //change button text
	$('#btnDelete').text('Process...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable 
    var url;
	
 
    if(method == 'add') {
        url = 'home/ajax_add';
    } else if (method == 'update') {
        url = 'home/ajax_update';
    } else {
		url = 'home/ajax_delete';
	}
 
    // ajax adding data to database
    $.ajax({
        url : url,
        type: "POST",
        data: $('#formModalAdd').serialize(),
        dataType: "JSON",
        success: function(data)
        {
            if(data.status) //if success close modal and reload ajax table
            {
                $('#myModal').modal('hide');
                $('#myModalDelete').modal('hide');
                reload_table();
                notifSuccess(data);
            }
            else
            {
                for (var i = 0; i < data.inputerror.length; i++) 
                {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                }
            }
            $('#btnSave').text('Submit'); //change button text
			$('#btnDelete').text('Delete'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
 
 
        },
        error: function (jqXHR, textStatus, errorThrown)
        {
            notifError(); 
            $('#btnSave').text('Submit'); //change button text
			$('#btnDelete').text('Delete'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable 
 
        }
    });
}

/**
** Refresh listdata
**/
function reload_table()
{
    table.ajax.reload(null,false); //reload datatable ajax 
}


function notifSuccess(data){
   new PNotify({
      title: data.title,
      text:  data.text,
      type:  data.type,
      animate: {
          animate: true,
          in_class: 'slideInDown',
          out_class: 'slideOutUp'
      }
  });
}
function notifError(){
   new PNotify({
      title: 'Failed!',
      text:  'Sorry there was an error in process data',
      type:  'error',
      animate: {
          animate: true,
          in_class: 'slideInDown',
          out_class: 'slideOutUp'
      }
  });
}