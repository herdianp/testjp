$('#loginForm').on('click', '#dosubmit', function (event) {
	event.preventDefault();
	var redirect = $("#redirect").val();
	var param = $("#loginForm").serialize();
	$.ajax({
			url: $("#loginForm").attr("action"),
			type: $("#loginForm").attr("method"),
			data: param,
			dataType: "json",
			success : function (data){
				if(data.status === false){
					hideshowalert("alert");
					$("#alert").html(data.message);
				}else{
					alert(data.message);
					window.location = redirect;
				} 		
		    }
	});    
}); 

function hideshowalert(value){
	var div = $("#"+value);
	div.fadeIn("slow").removeClass("show").addClass("hide");
	div.fadeIn("slow").removeClass("hide").addClass("show");
}