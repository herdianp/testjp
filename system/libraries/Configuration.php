<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class CI_Configuration {
	function __construct(){
		$this->ci = & get_instance();
		$this->ci->load->model("airnav_model"); //load model users
	}
	
	function load_image($image){
		$show ="<img src=\"".$image."\" class=\"img-rounded\" width=\"150px\"/>";		
		return $show;
		
	}
	
	function load_miss_page($result, $page, $data){
		if($result){
			$dataload = $this->ci->load->view($page, $data, TRUE);
		}else{
			$data['heading'] = 'Missing';
			$data['message'] = 'User Not Found';
			$dataload = $this->ci->load->view('missing', $data, TRUE);
		}	
		return $dataload;
	}
	
	function load_modal(){
		$html = "<!-- Modal HTML -->
				<div id=\"myModal\" class=\"modal fade\">
					<div class=\"modal-dialog\">
						<div class=\"modal-content\">
						
						</div>
					</div>
				</div>";
		return $html;		
	}
	
	function get_group($groupid){
		$datadb = $this->ci->airnav_model->get_array_data("usr_group");
		$grouplist = $datadb;
		//$grouplist = array('0' => "User",
		//				   '1' => "Administratos",
		//				   '2' => "QC");
		var_dump($grouplist); 
		//return $grouplist[$groupid];				   
		
	}
	
	function list_button(){
		$buttonlist = array('edit' => 'fa-edit', 
							'update' => 'fa-check', 
							'delete' => 'fa-remove', 
							'active' => 'fa-unlock', 
							'inactive' => 'fa-lock',
							'create' => 'fa-plus',							
							'refresh' => 'fa-refresh',
							'apply' => 'fa-check');
		return $buttonlist;		
	}
	
	function cek_button($button){
		$buttonlist = $this->list_button();
		
		if(array_key_exists($button, $buttonlist)){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	function button($name, $icon, $button, $position, $url){
		$buttonlist = $this->list_button();
		$iconbutton = $buttonlist[$icon];
		$btn = "<a type=\"button\" class=\"btn btn-default ".$button."\" data-toggle=\"tooltip\" data-placement=\"".$position."\" title=\"".$name."\" href=\"".$url."\" ><i class=\"fa ".$iconbutton." \"></i></a>\n";
		return $btn;
	}
}